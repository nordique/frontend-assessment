"use strict";

// Dependencies
var gulp = require('gulp');
var argv = require('yargs').argv;
var sass = require('gulp-sass')(require('node-sass'));
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var browserSync = require('browser-sync');
var connect = require("gulp-connect-php");
var notify = require("gulp-notify");
var gulpif = require('gulp-if');
var cleanCSS = require('gulp-clean-css');

var paths = {
  dirs: {
    root: './',
    build: './build',
    assets: './assets',
    css_build: './build/css/',
    js_build: './build/js/',
  },
  js: './assets/js/functions.js',
  scss: './assets/scss/style.scss',
  maps: './assets/maps',
};

// Change VHOST url here for browserSync
var vhost_url = 'www.frontend-assessment.test'

// Gulp browserSync
var reload = browserSync.reload;

gulp.task('browser-sync', (done) => {
  browserSync({
    open: 'external',
    host: vhost_url,
    proxy: vhost_url, // or project.dev/app/
    port: 3000,
    snippetOptions: {
      ignorePaths: ["core/**", 'wp-admin/**']
    }
  });
  connect.server({ base: vhost_url, port: 3000, keepalive: true });
  done();
});

// Gulp SASS
gulp.task('sass', () => {
  return gulp.src(paths.scss)
    .pipe(gulpif(argv.production, sass({ outputStyle: 'compressed' }).on('error', notify.onError({
      message: "<%= error.message %>",
      title: "Sass Error"
    }))))
    .pipe(gulpif(!argv.production, sass({ outputStyle: 'expanded' }).on('error', notify.onError({
      message: "<%= error.message %>",
      title: "Sass Error"
    }))))
    .pipe(gulpif(argv.production, rename({ extname: '.min.css' })))
    .pipe(postcss([autoprefixer('last 4 versions', 'ie >= 10', 'Safari >= 9')]))
    .pipe(cleanCSS({ compatibility: 'ie9', level: 2 }))
    .pipe(gulp.dest(paths.dirs.css_build))  // style.min.css to build
    .pipe(gulpif(!argv.production, browserSync.stream({ match: '**/style.css' })), gulpif(argv.production, browserSync.stream({ match: '**/style.min.css' })))
});

// Gulp JS
gulp.task('js', function () {
  return gulp.src([paths.js])
    .pipe(concat('main.js'))
    .pipe(gulpif(argv.production, rename({ extname: '.min.js' })))
    .pipe(gulpif(argv.production, uglify({
      mangle: true,
      compress: {
        drop_console: true
      }
    }).on('error', notify.onError({
      message: "<%= error.message %>",
      title: "JS Error"
    }))))
    .pipe(gulp.dest(paths.dirs.js_build)); // main.min.js to build
});

// Gulp watch
gulp.task('serve', gulp.series('sass', 'js', 'browser-sync', (done) => {
  gulp.watch("assets/scss/**/*.scss", gulp.series('sass'));
  gulp.watch('assets/js/**/*.js', gulp.series('js'));
}));
