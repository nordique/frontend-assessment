# Frontend Assessment
Dit is een simpele opzet voor kleine testprojecten. 

## De opdracht
De bedoeling van dit assessment is dat je het aangeleverde design overneemt in een landingspagina.
De landingspagina moet voor zowel desktop als mobiel gestyled worden, mobiel ontwerp is hier naar eigen inzicht.

We willen graag zien dat de pagina steeds na 10 seconden automatisch herladen wordt, daarnaast moet de timer in het design ook aftellen.
Het moet ook mogelijk zijn om de pagina te herladen door op de daarvoor bedoelde knop te klikken.

## Requirements

### Node & NPM
Dit project maakt gebruik van Node en NPM voor het compileren van de styling en Javascript. Dit project maakt gebruik van `Node 10`. Mocht je deze versie niet geïnstalleerd hebben kun je dat doen door middel van NVM (https://github.com/nvm-sh/nvm). 

NVM is een tool om gemakkelijk verschillende Node en NPM versies te installeeren en hier tussen te wisselen.

### Lokale ontwikkelomgeving
Bij Nordique gebruiken wij Valet (https://laravel.com/docs/8.x/valet) voor onze lokale ontwikkel omgevingen. Je kan ook gebruik maken van bijvoorbeeld MAMP (https://www.mamp.info/en/downloads/).

### Sketch
Het ontwerp is gemaakt in Sketch. Sketch biedt een proefperiode van 30 dagen, deze kun je gebruiken om het ontwerp te openen. Heb je geen toegang (meer) tot de proefperiode dan kun je Zeplin gebruiken, hierin kun je het Sketch bestand openen.

## Installatie
1. Plaats de inhoud van het zip-bestand in een project map zodat je hierin kan werken
2. Als je geen `Node 10` gebruikt dien je deze te installeren/ in te stellen. Dit kun je doen door `nvm install 10` en `nvm use 10` respectievelijk.  
3. Installeer de benodigde dependencies door `npm install` in de root van het project te doen.
4. Pas de URL in `gulpfile.js` aan naar de door jouw gekozen domein- / projectnaam.
5. Gebruik `gulp serve` om de compiler te starten. Krijg je een foutmelding dat Gulp nog niet geïnstalleerd is ondanks de `npm install` in stap 3, voer dan `npm install -g gulp` uit. 

Succes!
